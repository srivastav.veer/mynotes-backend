const Sequelize = require('sequelize');
let sequelize = require("../../config/DBConnect");

let columns = {
    id: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'id'
    },
    user_id: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
        field: 'user_id'
    },
    topic_name: {
        type: Sequelize.STRING(200),
        allowNull: false,
        field: 'topic_name'
    },
    topic_type: {
        type: Sequelize.STRING(50),
        allowNull: false,
        field: 'topic_type'
    },
    details: {
        type: Sequelize.TEXT,
        allowNull: true,
        field: 'details'
    },
    created_at: {
        type: Sequelize.BIGINT(10),
        allowNull: false,
        field: 'created_at'
    },
    updated_at: {
        type: Sequelize.BIGINT(10),
        allowNull: true,
        field: 'updated_at'
    },
    deleted_at: {
        type: Sequelize.BIGINT(10),
        allowNull: true,
        field: 'deleted_at'
    }
};

let table = {
    tableName: 'topics',
    timestamps: false
}

const Topics = sequelize.define('topics', columns, table);
module.exports = Topics;