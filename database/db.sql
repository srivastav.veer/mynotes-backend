CREATE TABLE `users` (
  `id` bigint(11) PRIMARY KEY auto_increment,
  `email_id` varchar(100) UNIQUE NOT NULL,
  `first_name` varchar(50),
  `last_name` varchar(50),
  `password` varchar(200) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `login_auth` varchar(200) DEFAULT null,
  `last_login` bigint(11) DEFAULT null,
  `created_at` bigint(11) NOT NULL,
  `updated_at` bigint(11) DEFAULT null,
  `deleted_at` bigint(11) DEFAULT null
);

CREATE TABLE `topics` (
  `id` bigint(11) PRIMARY KEY auto_increment,
  `user_id` bigint(11) NOT NULL,
  `topic_name` varchar(200) NOT NULL,
  `topic_type` varchar(50) NOT NULL,
  `details` text,
  `created_at` bigint(11) NOT NULL,
  `updated_at` bigint(11) DEFAULT null,
  `deleted_at` bigint(11) DEFAULT null
);

ALTER TABLE `topics` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
