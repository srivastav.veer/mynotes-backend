let models = {};

models.User = require('./models/Users');
models.Topic = require('./models/Topics');

models.User.hasMany(models.Topic, {
    as: 'UserTopics',
    foreignKey: 'user_id'
});

models.Topic.belongsTo(models.User, {
    as: 'UserOfTopic',
    foreignKey: 'user_id'
});

module.exports = models;