var express = require('express');
var router = express.Router();

var UserService = require('../services/UserService');
var UserAuthenticate = require('../middleware/UserAuthenticate')

router.post('/login', UserService.login); //User Login
router.post('/', UserService.signup); //User Signup
router.post('/verifyAuth', UserService.verifyAuth); //Verify if user is loggedin or not.

router.put('/', UserAuthenticate, UserService.update_user_details); //update profile details not password
router.put('/password', UserAuthenticate, UserService.update_password); //Update password of the account.

module.exports = router;