var express = require('express');
var router = express.Router();
var UserAuthenticate = require('../middleware/UserAuthenticate')

var TopicService = require('../services/TopicService');

router.get('/search-topic/:search_key', UserAuthenticate, TopicService.search_topics); //Search topic
router.post('/', UserAuthenticate, TopicService.create_topic); //Create a new topic
router.get('/', UserAuthenticate, TopicService.get_all_topics); //get all topics for the user
router.get('/:topicId', UserAuthenticate, TopicService.get_topic_by_id); //get a particular topic by id
router.put('/:topicId', UserAuthenticate, TopicService.updateTopic); //get a particular topic by id

module.exports = router;