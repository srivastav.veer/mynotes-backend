let models = require('../database/association');
const jwt = require("jsonwebtoken");
require('dotenv').config();

var UserAuthenticate = (req, res, next) => {
	let decoded = null;
	let token = req.header('x-auth');
	console.log(token);

	try {
		decoded = jwt.verify(token, process.env.JWT_SECRET);
	} catch (e) {
		res.status(401).send('Your Session has expired');
		res.end();
		return;
	}
	
	models.User.findOne({
		where: {
			id: decoded.id,
            login_auth: token,
		}
	}).then(user => {
		if(!user) {
			res.status(401).send({message: 'Your credentials are invalid'});
			res.end();
			return;
		}

		req.user = user;
		next();

	}).catch(e=> {
        console.log(e)
		res.status(405).send('Some Error Occured');
	})
};

module.exports = UserAuthenticate;