require('dotenv').config();

const cryptojs = require('crypto-js');

let validator = require('../utility/validator');
let UserController = require('../controllers/UserController');
let utility = require('../utility/utility');
let models = require('../database/association');
let Op = require('sequelize').Op;

var UserService = {
    login: function (req, res) {
        let data = {
            email_id: req.body.email,
            password: req.body.password
        }

        UserController.getUserAuth(data).then(user => {
            res.status(200).send({token: user.login_auth, data: user, message: 'Login Successful'});
        }).catch(e => {
            console.log(e)
            res.status(e.status).send({message: e.message});
        });
    },

    verifyAuth: function (req, res) {
        let data = {
            id: req.body.userId,
            token: req.body.token
        }
        UserController.verifyUserAuth(data).then(user => {
            res.status(200).send({token: user.login_auth, data: user, message: 'Login Successful'});
        }).catch(e =>{
            res.status(e.status).send({message: e.message});
        });
    },

    signup: function (req, res) {
        if (!('first_name' in req.body)) {
            res.status(400).send({message: 'First name is missing'}).end();
            return false;
        }
        if (!('last_name' in req.body)) {
            res.status(400).send({message: 'Last name is missing'}).end();
            return false;
        }
        if (!('email' in req.body)) {
            res.status(400).send({message: 'Email id is missing'}).end();
            return false;
        } else {
            if (!validator.validateEmail(req.body.email)) {
                res.status(400).send({message: 'Invalid email id'}).end();
                return false;
            }
        }
        if (!('password' in req.body)) {
            res.status(400).send({message: 'Password is missing'}).end();
            return false;
        }
        if (req.body.password !== req.body.cpassword) {
            res.status(400).send({message: 'Password mismatch'}).end();
            return false;
        }

        let salt = cryptojs.MD5(utility.random_salt(5)).toString();

        let data = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email_id: req.body.email,
            password: cryptojs.MD5(req.body.password+salt+process.env.FIXED_SALT).toString(),
            salt: salt
        };

        UserController.createUser(data).then( d => {
            res.status(200).send({message: 'Signup Successful', data: d}).end();
        }).catch(e => {
            res.status(e.status).send({message: e.message}).end();
        })
    },

    update_user_details: async function (req, res) {
        if (!('first_name' in req.body)) {
            res.status(400).send({message: 'First name is missing'}).end();
            return false;
        }
        if (!('last_name' in req.body)) {
            res.status(400).send({message: 'Last name is missing'}).end();
            return false;
        }

        req.user.first_name = req.body.first_name;
        req.user.last_name = req.body.last_name;

        UserController.updateProfileDetails(req.user).then(user => {
            res.status(200).send({message: 'User details updated', data: user}).end();
        }).catch(e => {
            console.log('Something ',e)
            res.status(e.status).send({message: e.message}).end();
        })
    },

    update_password: function (req, res) {
        let currentPassword = req.body.current_password;
        if (req.body.new_password !== req.body.cnew_password) {
            return res.status(400).send({message: 'Password mismatch'}).end();
        }

        UserController.updatePassword(req.user.id, currentPassword, req.body.new_password).then(user => {
            res.status(200).send({message: 'User details updated', data: user}).end();
        }).catch(e => {
            res.status(e.status).send({message: e.message}).end();
        })
    },
};

module.exports = UserService;