require('dotenv').config();

let TopicController = require('../controllers/TopicController');

var TopicService = {
    search_topics: function( req, res) {
        TopicController.searchTopics(req.user.id, req.params.search_key, req.query.limit, req.query.offset).then(topics => {
            res.status( 200).send( topics)
        })
    },

    create_topic: function( req, res) {
        if(!req.body.topicName) {
            return res.status(404).send('Topic name not found')
        } else if (!req.body.details) {
            return res.status(404).send('Topic text not found')
        } else if (!req.body.topicType) {
            return res.status(404).send('Topic type not found')
        }

        TopicController.createTopic(req.user.id, req.body).then(topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    get_topic_by_id: function( req, res) {
        TopicController.getTopicById(req.user.id, req.params.topicId).then(topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    get_all_topics: function( req, res) {
        TopicController.getAllTopics(req.user.id, req.query.limit, req.query.offset).then( topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    updateTopic: function (req, res) {
        let data = {
            id: req.body.id,
            topic_name: req.body.topicName,
            details: req.body.details,
        }
        
        TopicController.updateTopicById(req.user.id, data).then( topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    }
}

module.exports = TopicService;