module.exports = {
    random_salt: function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    },
    dateToEpoch: function(dateString) {
        var someDate = new Date(dateString);
        someDate = someDate.getTime();
        return someDate;
    }
}
