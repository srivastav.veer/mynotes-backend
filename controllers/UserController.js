require('dotenv').config();
let models = require('../database/association'); //Include Models
let validator = require('../utility/validator'); //Validator
let utility = require('../utility/utility'); //Validator

const cryptojs = require('crypto-js'); //For Md5
let Sequelize = require('sequelize'); // for the lower() method

let CryptoJS = require("crypto-js");
let SHA256 = require("crypto-js/sha256");
let md5 = require('md5');

var generateOTP = function (data) {
    return (SHA256(md5(data)).toString(CryptoJS.enc.Hex)).replace(/\D/g, '').substr(1, 4);
};

const jwt = require("jsonwebtoken"); //For creating JWT Token
let Op = require('sequelize').Op;

var UserController = {

    getUserById: function(id) {
        return new Promise( function( resolve, reject) {
            models.User.findOne({
                attributes: ['id', 'email_id', 'username', 'first_name', 'last_name'],
                where: {
                    id: id
                }
            }).then(user => {
                if (!user) {
                    return reject({status: 404, message: 'User not found.'});
                }
                return resolve({status: 200, message: 'OK', data: user});
            }).catch(e => {
                console.log(e)
                return reject({status: 500, message: 'Database Error', data: e});
            });
        });
    },

    getUserByEmail: function(email) {
        return new Promise( function( resolve, reject) {
            models.User.findOne({
                attributes: ['id', 'email_id', 'username', 'first_name', 'last_name'],
                where: {
                    email_id: email
                }
            }).then(user => {
                if (!user) {
                    return reject({status: 404, message: 'User not found.'});
                }
                return resolve({status: 200, message: 'OK', data: user});
            }).catch(e => {
                return reject({status: 500, message: 'Database Error', data: e});
            });
        });
    },

    getUsers: function(limit, offset) {
        limit = limit || 20;
        offset = offset || 0;
        return new Promise( function( resolve, reject) {
            models.User.findAll({
                attributes: ['id', 'email_id', 'username', 'first_name', 'last_name'],
                limit: limit,
                offset: offset
            }).then(users => {
                if (!users) {
                    return reject({status: 404, message: 'User not found.'});
                }
                return resolve({status: 200, message: 'OK', data: users});
            }).catch(e => {
                return reject({status: 500, message: 'Database Error', data: e});
            });
        });
    },

    updateProfileDetails: function (thisuser) {
        return new Promise(function (resolve, reject) {
            this.getUserById(thisuser.id).then(data => {
                let user = data.data
                user.first_name = thisuser.first_name;
                user.last_name = thisuser.last_name;
                // user.email_id = thisuser.email;

                user.save().then(() => {
                    return resolve({status: 200, message: 'User details updated', data: user});
                }).catch(e => {
                    return reject({status: 500, message: 'Database Error', data: e});
                })
            }).catch(e => {
                return reject({status: e.status, message: e.message, data: e});
            })
        }.bind(this));
    },

    updatePassword: function(userId, oldPassword, newPassword, isUpdate = true) {
        return new Promise( function( resolve, reject) {
            models.User.findOne({
                where: {
                    id: userId
                }
            }).then(user => {
                if (!user) {
                    return reject({status: 401, message: 'User not found.'});
                }

                if (cryptojs.MD5(oldPassword + user.salt + process.env.FIXED_SALT).toString() !== user.password && isUpdate) {
                    return reject({status: 401, message: 'Password mismatch.'});
                }

                let salt = cryptojs.MD5(utility.random_salt(5)).toString();
                let passCrypt = cryptojs.MD5(newPassword + salt + process.env.FIXED_SALT).toString();

                user.salt = salt;
                user.password = passCrypt;

                user.save().then(() => {
                    delete user.password;
                    delete user.salt;
                    return resolve({status: 200, message: 'Password Updated', data: user});
                });
            }).catch(e => {
                console.log(e)
                reject({ status: 500, message: 'Some error occured'})
            })
        });
    },

    verifyUserAuth: function (data) {
        return new Promise( function( resolve, reject) {
            let decoded = null;
            let token = data.token;

            try {
                decoded = jwt.verify(token, process.env.JWT_SECRET);
            } catch (e) {
                return reject({status: 401, message: 'Your Session has expired'});
            }
            
            models.User.findOne({
                where: {
                    id: decoded.id,
                    login_auth: token
                },
                include: [{ 
                    model: models.Topic,
                    as: 'UserTopics'
                }]
            }).then(user => {
                if(!user) {
                    return reject({status: 401, message: 'Your credentials are invalid'});
                }
                return resolve({status: 200, message: 'Login Successful', data: user});
            }).catch(e=> {
                return reject({status: 500, message: 'Database Error'});
            })
        });
    },

    getUserAuth: function(data) {
        return new Promise( function( resolve, reject) {
            /**
             * Algorithm. 
             * 1. Validate Email
             *      FALSE. Reject from here
             * 2. Query the database with the email id.
             *      IF NOT FOUND. Reject.
             *      IF FOUND. 
             *          a. Create MD5(provided password+salt+env password token) and match with password stored in DB
             *              IF NOT MATCH. Reject.
             *              IF MATCH.
             *                  update last_login, create jwt token
             */
            if (!validator.validateEmail(data.email_id)) {
                console.warn('Email ',data)
                return reject({status: 401, message: 'Invalid email id'});
            }
            models.User.findOne({
                where: {
                    email_id: data.email_id
                },
                include: [{ 
                    model: models.Topic,
                    as: 'UserTopics'
                }]
            }).then(user => {
                if (!user) {
                    return reject({status: 401, message: 'User not found.'});
                }

                if (cryptojs.MD5(data.password + user.salt + process.env.FIXED_SALT).toString() !== user.password) {
                    return reject({status: 401, message: 'Password mismatch.'});
                }

                //Create JWT Token
                let token = jwt.sign({
                    id: user.id,
                    exp: Math.floor( Date.now() / 1000) + (86400 * 60) //2 Months
                }, process.env.JWT_SECRET).toString();

                let loginTime = new Date();
                loginTime = parseInt(loginTime.getTime()/1000);

                user.login_auth = token;
                user.last_login = loginTime;
                user.save().then(() => {
                    delete user.password;
                    delete user.salt;
                    return resolve({status: 200, message: 'Login Successful', data: user});
                });
            }).catch(e => {
                return reject({status: 500, message: 'Database Error', data: e});
            });
        });
    },

    createUser: function (data) {
        return new Promise (function (resolve, reject) {
            /**
             * Algorithm:
             * 
             * 1. Look if the user exist or not.
             *      YES. Send a message and reject saying the user already exist
             * 2. Create a new user and Send an email to the email id. -> resolve.
             */
            
            models.User.findOne({
                where: {
                    email_id: data.email_id
                }
            }).then(user => {
                console.log('User',user);
                if (user) {
                    return reject({status: 401, message: 'User already exist'});
                }

                let createdAt = new Date();
                createdAt = parseInt(createdAt.getTime()/1000);

                models.User.create({
                    email_id: data.email_id,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    password: data.password,
                    salt: data.salt,
                    login_auth: null,
                    last_login: null,
                    created_at: createdAt
                }).then(user => {
                    resolve({status: 200, message: 'User created successfully', data: user});
                }).catch(e => {
                    console.log(e)
                    return reject({status: 500, message: 'Database error while registering the user', data: e});
                });
            }).catch(e => {
                return reject({status: 500, message: 'Database Error', data: e});
            })
        });
    }
}

module.exports = UserController;