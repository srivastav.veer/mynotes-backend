require('dotenv').config();
let models = require('../database/association'); //Include Models
let validator = require('../utility/validator'); //Validator
let utility = require('../utility/utility'); //Validator
let Sequelize = require('sequelize');
let Op = Sequelize.Op;

var TopicController = {
    searchTopics: function(user_id, searchKey, limit, offset) {
        limit = parseInt(limit) || 20;
        offset = parseInt(offset) || 0;
        searchKey = searchKey || null;
        return new Promise( function( resolve, reject) {
            models.Topic.findAll({
                attributes: ['id', 'user_id', 'topic_name', 'details', 'created_at'],
                limit: limit,
                offset: offset,
                where: {
                    user_id: user_id,
                    deleted_at: {
                        [Op.ne]: null
                    },
                    [Op.and]: [
                        Sequelize.where(
                            Sequelize.fn('lower', Sequelize.col('topic_name')),
                            {
                                [Op.like]: '%'+searchKey.toLowerCase()+'%'
                            }
                        )
                    ]
                },
                order: [
                    [ 'updated_at', 'DESC']
                ]
            }).then(topics => {
                resolve(topics)
            }).catch(e => {
                console.log(e)
                reject({ err: 500, message: 'Some error occured'})
            })
        })
    },

    getAllTopics: function(user_id, limit, offset) {
        limit = parseInt(limit) || 20;
        offset = parseInt(offset) || 0;
        return new Promise( function( resolve, reject) {
            models.Topic.findAll({
                attributes: ['id', 'user_id', 'topic_name', 'topic_type', 'details', 'created_at'],
                limit: limit,
                offset: offset,
                where: {
                    deleted_at: {
                        [Op.ne]: null
                    },
                    user_id: user_id
                },
                order: [
                    [ 'updated_at', 'DESC']
                ]
            }).then(topics => {
                resolve(topics)
            }).catch(e => {
                console.log(e)
                reject({ err: 500, message: 'Some error occured'})
            })
        })
    },

    createTopic: function(userId, data) {
        return new Promise( function( resolve, reject) {
            models.Topic.create({
                user_id: userId,
                topic_name: data.topicName,
                details: data.details,
                topic_type: data.topicType,
                created_at: Math.floor(Date.now() / 1000)
            }).then(topic => {
                resolve(topic)
            }).catch(e => {
                console.log(e);
                reject({err: 500, message: 'Some error occured'})
            })
        })
    },

    getTopicById: function(user_id, topicId) {
        return new Promise( function( resolve, reject) {
            models.Topic.findOne({
                where: {
                    id: topicId,
                },
                user_id: user_id
            }).then(topic => {
                if (!topic) {
                    return reject({ err: 404, message: "Topic not found"})
                }
                resolve(topic)
            }).catch(e => {
                reject({ err: 500, message: "Some error occured"})
            })
        })
    },

    updateTopicById: function (user_id, data) {
        console.log("\n\n\n", data,"\n\n\n");
        return new Promise(function (resolve, reject) {
            models.Topic.findOne({
                where: {
                    id: data.id,
                    user_id: user_id
                }
            }).then(topic => {
                if (!topic) {
                    return reject({status: 404, message: 'Topic not found'});
                }
                let updated_at = new Date();
                updated_at = parseInt(updated_at.getTime()/1000);

                topic.topic_name = data.topic_name;
                topic.details = data.details;
                topic.updated_at = updated_at

                topic.save().then(() => {
                    return resolve({status: 200, message: 'Updated', data: topic});
                })
            })
        });
    }
}

module.exports = TopicController;